var path = require('path')
var config = require('../config')
var utils = require('./utils')
var projectRoot = path.resolve(__dirname, '../')
var webpack = require('webpack')

var config = {
  addVendor: function (name, path) {
    this.resolve.alias[name] = path;
    this.module.noParse.push(new RegExp(path));
  },
  entry: {
    app: './src/main.js',
    vendors: ['jquery', 'moment', 'clndr']
  },
  output: {
    path: config.build.assetsRoot,
    publicPath: config.build.assetsPublicPath,
    filename: '[name].js'
  },
  resolve: {
    extensions: ['', '.js', '.vue'],
    fallback: [path.join(__dirname, '../node_modules')],
    alias: {
      'src': path.resolve(__dirname, '../src'),
      'assets': path.resolve(__dirname, '../src/assets'),
      'components': path.resolve(__dirname, '../src/components')
    }
  },
  resolveLoader: {
    fallback: [path.join(__dirname, '../node_modules')]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendors', utils.assetsPath('js/vendors.[hash:8].js')),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /mn/)
  ],
  module: {
    noParse: [],
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        loader: 'babel',
        include: projectRoot,
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.html$/,
        loader: 'vue-html'
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: utils.assetsPath('img/[name].[hash:7].[ext]')
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
        }
      }
    ]
  },
  vue: {
    loaders: utils.cssLoaders()
  }
};

config.addVendor('siimple.css', path.resolve(__dirname, '../bower_components/siimple/dist/siimple.min.css'));
config.addVendor('jquery', path.resolve(__dirname, '../bower_components/jquery/dist/jquery.min.js'));
config.addVendor('underscore', path.resolve(__dirname, '../bower_components/underscore/underscore-min.js'));
config.addVendor('moment', path.resolve(__dirname, '../bower_components/moment/moment.js'));
config.addVendor('clndr', path.resolve(__dirname, '../bower_components/clndr/clndr.min.js'));

module.exports = config;
