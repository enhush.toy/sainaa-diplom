import router from '../router'

const API_URL = 'http://localhost:3001/'
const LOGIN_URL = API_URL + 'login'
const PROFILE_URL = API_URL + 'profile'
const WORK_SCHEDULE_URL = API_URL + 'work-schedule'
const SURVEY_SCHEDULE_URL = API_URL + 'survey-schedule'

export default {
  PROFILE_URL,
  WORK_SCHEDULE_URL,
  user: {
    authenticated: false,
    data: null
  },
  checkAuth() {
    const jwt = localStorage.getItem('id_token')
    this.user.authenticated = jwt ? true : false
  },
  login(context, credentials, redirect) {
    context.$http.post(LOGIN_URL, credentials)
    .then(({data}) => {
      localStorage.setItem('id_token', data.id_token)
      this.user.authenticated = true
      this.user.data = data.data
      if (redirect) {
        router.go(redirect)
      }
    }, () => {
      context.error = true
    })
  },
  getAuthHeader() {
    return {
      'Authorization': 'Bearer ' + localStorage.getItem('id_token')
    }
  },
  logout() {
    localStorage.removeItem('id_token')
    this.user.authenticated = false
  },
}
