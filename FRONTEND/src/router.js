import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './components/Home'
import Login from './components/Login'

Vue.use(VueRouter)

const router = new VueRouter({
  hashbang: false,
  history: true
})

router.map({
  '/': {
    component: Home
  },
  '/login': {
    component: Login
  }
})

router.redirect({
  '*': '/'
})

router.beforeEach((transition) => {
  // if (true) {
  //   redirect('/login')
  // }
  transition.next()
})

export default router
