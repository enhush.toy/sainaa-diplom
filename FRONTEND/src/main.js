import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './components/App'
import router from './router'
import Auth from './auth/index'

Vue.use(VueResource)

Vue.http.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');

Auth.checkAuth()

router.start(App, '#app')
