var express = require('express'),
    jwt     = require('express-jwt'),
    config  = require('./config'),
    quoter  = require('./quoter'),
    db = require('./db')

var app = module.exports = express.Router();

var jwtCheck = jwt({
  secret: config.secret
});

app.use('/profile', jwtCheck);
app.use('/work-schedule', jwtCheck);

app.get('/profile', function(req, res) {
  db.getProfile(req.user.id, function (err, result) {
    if (err || result.length <= 0) {
      return res.sendStatus(401)
    }
    return res.json({name: result[0].name})
  })
});

app.get('/work-schedule', function(req, res) {
  db.getWorkSchedule(req.user.id, function (err, result) {
    if (err || result.length <= 0) {
      return res.sendStatus(401)
    }
    return res.json(result[0])
  })
});

app.get('/survey-schedule', function(req, res) {
  db.getSurveySchedule(req.user.id, function (err, result) {
    if (err || result.length <= 0) {
      return res.sendStatus(401)
    }
    return res.json(result[0])
  })
});
