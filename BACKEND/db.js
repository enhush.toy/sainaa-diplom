const mysql = require('mysql')
const connection = mysql.createConnection({
  host : 'localhost',
  user : 'root',
  password : 'pass',
  database: 'hospital'
})

connection.connect()

const db = {
  getSurveySchedule(id, callback) {
    connection.query()
  },
  getWorkSchedule(id, callback) {
    connection.query("SELECT "+
	    "(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day1 FROM tb_doc_schedule WHERE DAY=1 AND DOCPKID = t1.DOCPKID) AS 'Даваа', "+
    	"(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day2 FROM tb_doc_schedule WHERE DAY=2 AND DOCPKID = t1.DOCPKID) AS 'Мягмар', "+
    	"(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day3 FROM tb_doc_schedule WHERE DAY=3 AND DOCPKID = t1.DOCPKID) AS 'Лхагва', "+
    	"(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day4 FROM tb_doc_schedule WHERE DAY=4 AND DOCPKID = t1.DOCPKID) AS 'Пүрэв', "+
    	"(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day5 FROM tb_doc_schedule WHERE DAY=5 AND DOCPKID = t1.DOCPKID) AS 'Баасан', "+
    	"(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day6 FROM tb_doc_schedule WHERE DAY=6 AND DOCPKID = t1.DOCPKID) AS 'Бямба', "+
    	"(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day7 FROM tb_doc_schedule WHERE DAY=7 AND DOCPKID = t1.DOCPKID) AS 'Ням' "+
      "FROM tb_doc_schedule AS t1 WHERE DOCPKID = ?",
      [id],
      (err, result) => {
        callback(err, result)
      }
    )
  },
  getProfile(id, callback) {
    connection.query(`select CONCAT(LEFT(FIRSTNAME, 1), '.',LASTNAME) AS name from tb_userinfo WHERE USERPKID = ?`,
      [id],
      (err, result) => {
        callback(err, result)
      }
    )
  },
  getUser(user, callback) {
    connection.query(`select * from tb_userinfo where USERNAME = ? and PASSWORD = ?`,
      [user.username, user.password],
      (err, result) => {
        callback(err, result)
      }
    )
  }
}
        // connection.query("SELECT tb_userinfo.USERPKID as id,CONCAT(SUBSTRING(tb_userinfo.LASTNAME,1,1),'. ',tb_userinfo.FIRSTNAME) AS Эмч,"+
    	  //               "(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day1 FROM tb_doc_schedule WHERE DAY=1 AND DOCPKID = t1.DOCPKID) AS 'Даваа',"+
    	  //               "(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day2 FROM tb_doc_schedule WHERE DAY=2 AND DOCPKID = t1.DOCPKID) AS 'Мягмар',"+
    	  //               "(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day3 FROM tb_doc_schedule WHERE DAY=3 AND DOCPKID = t1.DOCPKID) AS 'Лхагва',"+
    	  //               "(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day4 FROM tb_doc_schedule WHERE DAY=4 AND DOCPKID = t1.DOCPKID) AS 'Пүрэв',"+
    	  //               "(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day5 FROM tb_doc_schedule WHERE DAY=5 AND DOCPKID = t1.DOCPKID) AS 'Баасан',"+
    	  //               "(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day6 FROM tb_doc_schedule WHERE DAY=6 AND DOCPKID = t1.DOCPKID) AS 'Бямба',"+
    	  //               "(SELECT IF(REST=1,'Амралттай',CONCAT(TIME_FORMAT(START, '%H:%i'),'-',TIME_FORMAT(END, '%H:%i') )) day7 FROM tb_doc_schedule WHERE DAY=7 AND DOCPKID = t1.DOCPKID) AS 'Ням'"+
        //                 "FROM tb_doc_schedule AS T1, tb_userinfo"+
        //                 "WHERE tb_userinfo.USERPKID=DOCPKID"+
        //                 "GROUP BY DOCPKID", function (err, result2) {
        //                   console.log(err, result2);
        //                   if (err || result.length <= 0){
        //                     callback('err');
        //                   }
        //                   else {
        //                     console.log(result2);
        //                     callback(null, {firstname: result[0].FIRSTNAME,
        //                                     lastname: result[0].LASTNAME,
        //                                     username: result[0].USERNAME,
        //                                     password: result[0].PASSWORD,
        //                                     });
        //                   }
        //                 });
module.exports = db
