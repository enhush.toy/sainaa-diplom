var express = require('express'),
    _       = require('lodash'),
    jwt     = require('jsonwebtoken'),
    config  = require('./config'),
    db = require('./db');

var app = module.exports = express.Router();

function createToken(user) {
  return jwt.sign(_.omit(user, 'password'), config.secret, { expiresIn: 60*10 });
}

app.post('/login', function(req, res) {
  var username = req.body.username;
  var password = req.body.password;

  if (!username || !password) {
    return res.sendStatus(400);
  }
  db.getUser({
    username,
    password,
  }, (err, result) => {
    // error
    if (err || result.length <= 0) {
      return res.sendStatus(401);
    }
    // result
    return res.json({
      id_token: createToken({
        username: result[0].USERNAME,
        id: result[0].USERPKID
      }),
      data: {
        firstname: result[0].FIRSTNAME,
        lastname: result[0].LASTNAME
      }
    })
  })
})
